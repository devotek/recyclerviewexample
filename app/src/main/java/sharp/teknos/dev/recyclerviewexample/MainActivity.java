package sharp.teknos.dev.recyclerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Student> students = new ArrayList<Student>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        Student student = new Student();
        student.setFirstName("Vasudev");
        student.setLastName("Kumaran");
        student.setRollNum("1000");
        student.setSubject("Math");
        students.add(student);

        student = new Student();
        student.setFirstName("Vasudev");
        student.setLastName("Kumaran");
        student.setRollNum("1000");
        student.setSubject("Math");
        students.add(student);

        student = new Student();
        student.setFirstName("Vasudev");
        student.setLastName("Kumaran");
        student.setRollNum("1000");
        student.setSubject("Math");
        students.add(student);

        student = new Student();
        student.setFirstName("Vasudev");
        student.setLastName("Kumaran");
        student.setRollNum("1000");
        student.setSubject("Math");
        students.add(student);

        student = new Student();
        student.setFirstName("Vasudev");
        student.setLastName("Kumaran");
        student.setRollNum("1000");
        student.setSubject("Math");
        students.add(student);

        student = new Student();
        student.setFirstName("Vasudev");
        student.setLastName("Kumaran");
        student.setRollNum("1000");
        student.setSubject("Math");
        students.add(student);

        StudentAdapter studentAdapter = new StudentAdapter(this,students);
        recyclerView.setAdapter(studentAdapter);
    }
}
