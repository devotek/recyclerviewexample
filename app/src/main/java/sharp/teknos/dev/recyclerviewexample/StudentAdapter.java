package sharp.teknos.dev.recyclerviewexample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder>{

    private ArrayList<Student> students = new ArrayList<Student>();
    private LayoutInflater layoutInflater;
    public StudentAdapter(Context context, ArrayList<Student> students){
        layoutInflater = LayoutInflater.from(context);
        this.students = students;
    }

    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = (View)layoutInflater.inflate(R.layout.stident_record_view,parent,false);

        StudentViewHolder studentViewHolder = new StudentViewHolder(view);

        return studentViewHolder;
    }

    @Override
    public void onBindViewHolder(StudentViewHolder holder, int position) {
        holder.firstNameView.setText(students.get(position).getFirstName());
        holder.subjectView.setText(students.get(position).getSubject());
        holder.viewBtn.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //
        }
    };

     class StudentViewHolder extends RecyclerView.ViewHolder{

        public TextView firstNameView;
        public TextView subjectView;
        public Button viewBtn;
        public StudentViewHolder(View itemView) {
            super(itemView);
            firstNameView = (TextView) itemView.findViewById(R.id.firstNameText);
            subjectView = (TextView) itemView.findViewById(R.id.subjectText);
            viewBtn = (Button)itemView.findViewById(R.id.viewBtn);
        }
    }
}
